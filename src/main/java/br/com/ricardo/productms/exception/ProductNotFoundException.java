package br.com.ricardo.productms.exception;

import javax.persistence.EntityNotFoundException;

public class ProductNotFoundException extends EntityNotFoundException {
	
	private static final long serialVersionUID = 1L;

	public ProductNotFoundException(String message) {
		super(message);
	}
	
	public ProductNotFoundException(Long id) {
		this(String.format("Produto de código %d não encontrado.", id));
	}
	
}
