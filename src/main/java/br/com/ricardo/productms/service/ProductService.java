package br.com.ricardo.productms.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ricardo.productms.exception.ProductNotFoundException;
import br.com.ricardo.productms.model.Product;
import br.com.ricardo.productms.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Transactional
	public Product update(Product product) {
				
		Optional<Product> productValidation = productRepository.findById(product.getId());
		if (!productValidation.isPresent()) {
			throw new ProductNotFoundException(product.getId());
		}
		
		return productRepository.save(product);

	}
	
	@Transactional
	public void destroy(Long id) {
				
		Optional<Product> productValidation = productRepository.findById(id);
		if (!productValidation.isPresent()) {
			throw new ProductNotFoundException(id);
		}
		
		productRepository.deleteById(id);

	}
	

}
