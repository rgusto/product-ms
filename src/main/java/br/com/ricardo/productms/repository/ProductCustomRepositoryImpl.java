package br.com.ricardo.productms.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.ricardo.productms.model.Product;

public class ProductCustomRepositoryImpl implements ProductCustomRepository {

	@PersistenceContext
	private EntityManager entityManager;	
	
	@Override
	public List<Product> search(String q, BigDecimal minPrice, BigDecimal maxPrice) {
		
		StringBuilder query = new StringBuilder();
		query.append("Select p from Product p where 1=1");
		
		if (q != null) {
			query.append(" and (name LIKE '%").append(q).append("%' or description LIKE '%").append(q).append("%')");
		}
		
		if (minPrice != null) {
			query.append(" and price >= ").append(minPrice.toString());
		}
		
		if (maxPrice != null) {
			query.append(" and price <= ").append(maxPrice.toString());
		}
		
		List<Product> productList = entityManager.createQuery(query.toString()).getResultList();
		
		return productList;
		
	}

}
