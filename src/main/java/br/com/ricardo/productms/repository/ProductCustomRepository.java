package br.com.ricardo.productms.repository;

import java.math.BigDecimal;
import java.util.List;

import br.com.ricardo.productms.model.Product;

public interface ProductCustomRepository {
	
	List<Product> search(String q, BigDecimal minPrice, BigDecimal maxPrice);

}
