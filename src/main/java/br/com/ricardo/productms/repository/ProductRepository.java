package br.com.ricardo.productms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ricardo.productms.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long>, ProductCustomRepository {
	
}
