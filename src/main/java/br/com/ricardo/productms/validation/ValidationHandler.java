package br.com.ricardo.productms.validation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.ricardo.productms.exception.ProductNotFoundException;

@RestControllerAdvice
public class ValidationHandler {

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseError handleArgumentNotValidException(MethodArgumentNotValidException exception) {
		ResponseError responseError = new ResponseError(400, exception.getMessage());
		return responseError;
	}
	
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseError handleProductNotFoundException(ProductNotFoundException exception) {
		ResponseError responseError = new ResponseError(404, exception.getMessage());
		return responseError;
	}

}