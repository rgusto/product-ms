package br.com.ricardo.productms.controller;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ricardo.productms.model.Product;
import br.com.ricardo.productms.repository.ProductRepository;
import br.com.ricardo.productms.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductService productService;

	@GetMapping
	public List<Product> list() {

		List<Product> products = productRepository.findAll();
		return products;

	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> findById(@PathVariable Long id) {
		Optional<Product> product = productRepository.findById(id);
		if (product.isPresent()) {
			return ResponseEntity.ok(product.get());
		}

		return ResponseEntity.notFound().build();
	}

	@GetMapping("/search")
	public List<Product> search(@RequestParam(required = false) String q,
			@RequestParam(name = "min_price", required = false) BigDecimal minPrice,
			@RequestParam(name = "max_price", required = false) BigDecimal maxPrice) {
		List<Product> products = productRepository.search(q, minPrice, maxPrice);
		return products;
	}

	@PostMapping
	@Transactional
	public ResponseEntity<Product> store(@RequestBody @Valid Product form, UriComponentsBuilder uriBuilder) {
		productRepository.save(form);

		URI uri = uriBuilder.path("/products/{id}").buildAndExpand(form.getId()).toUri();
		return ResponseEntity.created(uri).body(form);
	}

	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Product> update(@PathVariable Long id, @RequestBody @Valid Product form) {

		form.setId(id);
		productService.update(form);

		return ResponseEntity.ok(form);

	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<?> destroy(@PathVariable Long id) {

		productService.destroy(id);

		return ResponseEntity.ok(id);
	}

}
